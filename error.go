package slackapp

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/rs/zerolog/log"
)

var (
	ErrInternalServerError = SlackAppError{
		StatusCode: http.StatusBadRequest,
		Err: SlackAppAPIError{
			Code:    10000,
			Message: "internal server error",
		},
	}
	ErrMalformedInteractionCallback = SlackAppError{
		StatusCode: http.StatusBadRequest,
		Err: SlackAppAPIError{
			Code:    10001,
			Message: "malformed interactivity callback",
		},
	}
	ErrMalformedSlackCallback = SlackAppError{
		StatusCode: http.StatusBadRequest,
		Err: SlackAppAPIError{
			Code:    10002,
			Message: "malformed slack callback",
		},
	}
	ErrMalformedSlashCommand = SlackAppError{
		StatusCode: http.StatusBadRequest,
		Err: SlackAppAPIError{
			Code:    10003,
			Message: "malformed slash command",
		},
	}
	ErrMalformedEventCallback = SlackAppError{
		StatusCode: http.StatusBadRequest,
		Err: SlackAppAPIError{
			Code:    10004,
			Message: "malformed event callback",
		},
	}
	ErrUnsupportedSlackFeature = SlackAppError{
		StatusCode: http.StatusBadRequest,
		Err: SlackAppAPIError{
			Code:    10005,
			Message: "unsupported slack feature",
		},
	}
	ErrUnsupportedInteractionID = SlackAppError{
		StatusCode: http.StatusBadRequest,
		Err: SlackAppAPIError{
			Code:    10006,
			Message: "unsupported interaction ID",
		},
	}
	ErrUnsupportedSlashCommand = SlackAppError{
		StatusCode: http.StatusBadRequest,
		Err: SlackAppAPIError{
			Code:    10007,
			Message: "unsupported slash command",
		},
	}
	ErrMalformedDialogSubmission = SlackAppError{
		StatusCode: http.StatusBadRequest,
		Err: SlackAppAPIError{
			Code:    10008,
			Message: "malformed dialog submision paylod",
		},
	}
	ErrSlackAPIDialog = SlackAppError{
		StatusCode: http.StatusBadRequest,
		Err: SlackAppAPIError{
			Code:    10009,
			Message: "unable to create dialog",
		},
	}
)

type SlackAppAPIError struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}

type SlackAppError struct {
	StatusCode int
	Err        SlackAppAPIError `json:"error"`
}

func (e *SlackAppError) Error() string {
	return fmt.Sprintf("%d: %s", e.Err.Code, e.Err.Message)
}

func (e *SlackAppError) AsJson() string {
	enc, err := json.Marshal(e)
	if err != nil {
		log.Error().Err(err).Msg("Unable to marshal SlackAppError to JSON")
	}
	return string(enc)
}
