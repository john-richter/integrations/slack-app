package slackapp

type SlackConfig struct {
	SlackClientID          string `json:"slack_client_id" mapstructure:"slack_client_id"`
	SlackClientSecret      string `json:"slack_client_secret" mapstructure:"slack_client_secret"`
	SlackSigningSecret     string `json:"slack_signing_secret" mapstructure:"slack_signing_secret"`
	SlackVerificationToken string `json:"slack_verification_token" mapstructure:"slack_verification_token"`
	SlackBotToken          string `json:"slack_bot_token" mapstructure:"slack_bot_token"`
}

func NewDefaultSlackConfig() *SlackConfig {
	return &SlackConfig{
		SlackClientID:          "",
		SlackClientSecret:      "",
		SlackSigningSecret:     "",
		SlackVerificationToken: "",
		SlackBotToken:          "",
	}
}
