package slackapp

import (
	"context"

	"github.com/slack-go/slack"
	"github.com/slack-go/slack/slackevents"
)

type SlackService interface {
	ServiceName() string
	SlashCommands() []string
	InteractionIDs() []string
	EventTypes() []string
	HandleSlashCommand(ctx context.Context, cmd *slack.SlashCommand) (*SlackCallbackResponse, error)
	HandleInteraction(ctx context.Context, ic *slack.InteractionCallback) (*SlackCallbackResponse, error)
	HandleEvent(
		ctx context.Context,
		ce *slackevents.EventsAPICallbackEvent,
		ie *slackevents.EventsAPIInnerEvent,
	)
}
