package slackapp

import (
	"encoding/json"
	"net/http"

	"github.com/rs/zerolog/log"
)

var (
	SuccessNoContent = SlackCallbackResponse{StatusCode: http.StatusNoContent}
)

type SlackCallbackResponse struct {
	StatusCode int
	Headers    map[string]string
	Data       interface{}
}

func (r *SlackCallbackResponse) AsJson() string {
	enc, err := json.Marshal(r.Data)
	if err != nil {
		log.Error().Err(err).Msg("Unable to marshal SlackCallbackResponse to JSON")
	}
	return string(enc)
}

type SlackEventsApiUrlVerificationMessage struct {
	Challenge string `json:"challenge"`
}
