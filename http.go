package slackapp

import (
	"bytes"
	"io/ioutil"
	"net/http"
)

func CopyRequestBody(r *http.Request) []byte {
	var err error
	var bodyBytes []byte
	if r.Body != nil {
		bodyBytes, err = ioutil.ReadAll(r.Body)
		if err != nil {
			// TODO should return an error
			return []byte{}
		}
		r.Body.Close()
		r.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))
	}
	return bodyBytes
}
