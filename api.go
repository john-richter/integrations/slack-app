package slackapp

import (
	"context"
	"net/http"
	"sync"

	"github.com/rs/zerolog/log"
	"github.com/slack-go/slack"
	"github.com/slack-go/slack/slackevents"
	"gitlab.com/johnrichter/logging-go"
	traceext "gopkg.in/DataDog/dd-trace-go.v1/ddtrace/ext"
	"gopkg.in/DataDog/dd-trace-go.v1/ddtrace/tracer"
)

type slackAppServices struct {
	SlashCommands map[string]SlackService
	Interactions  map[string]SlackService
	Events        map[string][]SlackService
}

type SlackApp struct {
	session  *SlackSession
	services *slackAppServices
}

func NewSlackApp(sc *SlackConfig) *SlackApp {
	a := SlackApp{
		session: NewSlackSession(sc),
		services: &slackAppServices{
			SlashCommands: map[string]SlackService{},
			Interactions:  map[string]SlackService{},
			Events:        map[string][]SlackService{},
		},
	}
	return &a
}

func (a *SlackApp) RegisterService(s SlackService) {
	for _, sc := range s.SlashCommands() {
		if _, exists := a.services.SlashCommands[sc]; exists {
			log.Warn().Str(logging.SlackSlashCommandName, sc).Msg("Duplicate slash command registered for slack app")
		}
		a.services.SlashCommands[sc] = s
	}
	for _, i := range s.InteractionIDs() {
		if _, exists := a.services.Interactions[i]; exists {
			log.Warn().Str(logging.SlackInteractionID, i).Msg("Duplicate interaction ID registered for slack app")
		}
		a.services.Interactions[i] = s
	}
	for _, et := range s.EventTypes() {
		if curr, exists := a.services.Events[et]; exists {
			a.services.Events[et] = append(curr, s)
		} else {
			a.services.Events[et] = []SlackService{s}
		}
	}
}

func (a *SlackApp) HandleSlackCallback(feature string, r *http.Request) (*SlackCallbackResponse, error) {
	if !a.session.IsCallbackValid(r) {
		return nil, &ErrMalformedSlackCallback
	}
	switch feature {
	case SlackFeatureInteraction:
		ic, ok := a.session.GetInteractivityCallback(r)
		if !ok {
			return nil, &ErrMalformedInteractionCallback
		}
		return a.respondToInteraction(r.Context(), ic)

	case SlackFeatureSlashCommand:
		sc, ok := a.session.GetSlashCommand(r)
		if !ok {
			return nil, &ErrMalformedSlashCommand
		}
		return a.runSlashCommand(r.Context(), sc)

	case SlackFeatureEvent:
		event, ok := a.session.GetEvent(r)
		if !ok {
			return nil, &ErrMalformedEventCallback
		}
		return a.handleEvent(r.Context(), event)
	}
	log.Debug().Str(logging.SlackFeature, feature).Msg("Unknown slack feature")
	return nil, &ErrUnsupportedSlackFeature
}

func (a *SlackApp) respondToInteraction(
	ctx context.Context,
	ic *slack.InteractionCallback,
) (*SlackCallbackResponse, error) {

	span, _ := tracer.SpanFromContext(ctx)
	span.SetTag(traceext.ResourceName, "slack.interaction")
	defer span.Finish()

	if s, exists := a.services.Interactions[ic.CallbackID]; exists {
		return s.HandleInteraction(ctx, ic)
	}
	log.Debug().Str(logging.SlackInteractionID, ic.CallbackID).Msg("Unknown slack interaction ID")
	return nil, &ErrUnsupportedInteractionID
}

func (a *SlackApp) runSlashCommand(
	ctx context.Context,
	cmd *slack.SlashCommand,
) (*SlackCallbackResponse, error) {

	span, _ := tracer.SpanFromContext(ctx)
	span.SetTag(traceext.ResourceName, "slack.slash_command")
	defer span.Finish()

	if s, exists := a.services.SlashCommands[cmd.Command]; exists {
		return s.HandleSlashCommand(ctx, cmd)
	}
	log.Debug().Str(logging.SlackSlashCommandName, cmd.Command).Msg("Unknown slack slash command")
	return nil, &ErrUnsupportedSlashCommand
}

func (a *SlackApp) handleEvent(ctx context.Context, event *slackevents.EventsAPIEvent) (*SlackCallbackResponse, error) {
	span, _ := tracer.SpanFromContext(ctx)
	span.SetTag(traceext.ResourceName, "slack.event")
	defer span.Finish()

	switch ed := event.Data.(type) {
	case *slackevents.EventsAPIURLVerificationEvent:
		vr := SlackEventsApiUrlVerificationMessage{Challenge: ed.Challenge}
		return &SlackCallbackResponse{StatusCode: http.StatusOK, Data: vr}, nil

	case *slackevents.EventsAPIAppRateLimited:
		log.Warn().
			Str(logging.SlackTeamID, ed.TeamID).
			Str(logging.SlackAppID, ed.APIAppID).
			Int(logging.SlackApiRateLimitStart, ed.MinuteRateLimited).
			Msg("Slack application is rate limited")

	case *slackevents.EventsAPICallbackEvent:
		if svcs, exists := a.services.Events[event.InnerEvent.Type]; exists {
			var wg sync.WaitGroup
			for _, s := range svcs {
				wg.Add(1)
				go a.notifyServiceOfEvent(ctx, &wg, s, ed, &event.InnerEvent)
			}
			wg.Wait()
		} else {
			log.Debug().Str(logging.SlackEventType, event.InnerEvent.Type).Msg("Unsupported Slack event type")
		}
	default:
		log.Debug().Str(logging.SlackEventType, event.InnerEvent.Type).Msg("Unsupported Slack Events API type")
	}
	// If we return errors, Slack will rate limit us
	return &SlackCallbackResponse{StatusCode: http.StatusOK}, nil
}

func (a *SlackApp) notifyServiceOfEvent(
	ctx context.Context,
	wg *sync.WaitGroup,
	s SlackService,
	ce *slackevents.EventsAPICallbackEvent,
	ie *slackevents.EventsAPIInnerEvent,
) {
	defer wg.Done()
	s.HandleEvent(ctx, ce, ie)
}
